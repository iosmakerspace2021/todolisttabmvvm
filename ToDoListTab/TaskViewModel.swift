//
//  TaskViewModel.swift
//  ToDoListTab
//
//  Created by Olzhas Akhmetov on 12.04.2021.
//

import UIKit

struct State {
    enum EditingStyle {
        case addTask(String)
        case deleteTask(IndexPath)
        case toggleTask(IndexPath)
        case loadTasks([TaskItem])
        case none
    }
    
    private(set) var todolistarray: [TaskItem]
    var editingStyle: EditingStyle {
        didSet {
            switch editingStyle {
            case let .addTask(newTaskName):
                let newTask = TaskItem()
                newTask.task = newTaskName
                todolistarray.append(newTask)
            case let .deleteTask(indexPath):
                todolistarray.remove(at: indexPath.row)
            case let .toggleTask(indexPath):
                todolistarray[indexPath.row].isComplete = !todolistarray[indexPath.row].isComplete
            case let .loadTasks(todolistarray):
                self.todolistarray = todolistarray
            default:
                break
            }
        }
    }
    
    init(todolistarray: [TaskItem]) {
        self.todolistarray = todolistarray
        self.editingStyle = .none
    }
    
    func text(at indexPath: IndexPath) -> String {
        return todolistarray[indexPath.row].task
    }
    
    func isComplete(at indexPath: IndexPath) -> Bool {
        return todolistarray[indexPath.row].isComplete
    }
}

final class TaskViewModel {
    private(set) var state = State(todolistarray: []) {
        didSet {
            callback(state)
        }
    }
    let callback: (State) -> ()
    
    init(callback: @escaping (State) -> ()) {
        self.callback = callback
    }
    
    func addNewTask(taskName: String) {
        state.editingStyle = .addTask(taskName)
        saveTasks()
    }
    
    func deleteTask(at indexPath: IndexPath) {
        state.editingStyle = .deleteTask(indexPath)
        saveTasks()
    }
    
    func toggleTask(at indexPath: IndexPath) {
        state.editingStyle = .toggleTask(indexPath)
        saveTasks()
    }
    
    func accessoryType(at indexPath: IndexPath) -> UITableViewCell.AccessoryType {
        if state.isComplete(at: indexPath) {
            return .checkmark
        }
        return .none
    }
    
    func saveTasks() {
        let defaults = UserDefaults.standard
        
        do {
            let encodedData = try JSONEncoder().encode(state.todolistarray)
                
            defaults.set(encodedData, forKey: "taskArray")
        } catch {
            print("Unable to Encode Array (\(error))")
        }
    }
    
    func loadTasks() {
        let defaults = UserDefaults.standard
        
        do {
            if let data = defaults.data(forKey: "taskArray") {
                
                let array = try JSONDecoder().decode([TaskItem].self, from: data)
                
                state.editingStyle = .loadTasks(array)
            } else {
                
            }
        } catch {
            print("Unable to Encode Array (\(error))")
        }
    }
}
