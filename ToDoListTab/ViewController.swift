//
//  ViewController.swift
//  ToDoListTab
//
//  Created by Olzhas Akhmetov on 22.03.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    fileprivate var viewModel: TaskViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        viewModel = TaskViewModel { [unowned self] (state) in
        switch state.editingStyle {
                case .none:
                
                    break
                case .addTask(_):
                    textField.text = ""
                    break
                case .deleteTask(_):
                    
                    break
                case .toggleTask(_):
                    
                    break
                case .loadTasks(_):
                    
                    break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel?.loadTasks()
    }

    @IBAction func addTask(_ sender: Any) {
        viewModel?.addNewTask(taskName: textField.text!)
    }
    
}

